FROM rust:1.49 as builder
WORKDIR /usr/src/myapp
RUN rustup component add rustfmt --toolchain 1.49.0-x86_64-unknown-linux-gnu
COPY Cargo.toml .
COPY build.rs .
COPY proto proto
COPY src src
RUN cargo install --bin server --path .

FROM debian:buster-slim
# RUN apt-get update && apt-get install -y extra-runtime-dependencies && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/server /usr/local/bin/rqueue
CMD ["rqueue"]
