
REPO=localhost:5000
DB_URL="postgres://postgres:postgres@localhost"

build:
	cargo build

sql:
	env DATABASE_URL=$(DB_URL) cargo sqlx prepare

run:
	cargo run

docker:
	docker build --rm -t $(REPO)/rqueue .
	docker push $(REPO)/rqueue
