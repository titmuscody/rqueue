import pika


def callback(ch, method, properties, body):
     print(" [x] Received %r" % body)



connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost', credentials=pika.PlainCredentials('user', 'user')))
client = connection.channel()
client.queue_declare(queue='load_test')

client.basic_consume(
    queue='load_test',
    auto_ack=True,
    on_message_callback=callback,
)

client.start_consuming()
