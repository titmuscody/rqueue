package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"
	"os"

	"gitlab.com/titmuscody/rqueue/go_server/hello"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

const (
	// NAME of the queue
	NAME = "test topic"
)

func consumeItems(c hello.HelloClient) {
	md := metadata.New(map[string]string{"topic": NAME})
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	stream, err := c.ConsumeItems(ctx)
	if err != nil {
		log.Fatalf("Error when calling Send: %s", err)
	}

	ticker := time.NewTicker(5 * time.Second)
	endChan := make(chan bool)
	go func() {
		for {
			select {
			case <-ticker.C:
				err := stream.Send(&hello.ConsumerRequest{Id: 0, Status: ""})
				if err != nil {
					log.Fatalf("error sending ping %q", err)
				}
			case <-endChan:
				return
			}
		}
	}()

	for {
		ticker.Reset(10 * time.Second)
		item, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Error receiving stream %q", err)
		}
		ticker.Stop()
		log.Printf("received topic=%v message=%v", item.Id, item.Message)
		// time.Sleep(1 * time.Second)
		err = stream.Send(&hello.ConsumerRequest{Id: item.Id, Status: "ack"})
		if err != nil {
			log.Fatalf("Error sending stream %q", err)
		}
	}
	ticker.Stop()
	endChan <- true
}

func createQueue(c hello.HelloClient) {
	_, err := c.VerifyQueue(context.Background(), &hello.QueueProperties{Name: NAME, Durable: false})
	if err != nil {
		log.Fatalf("Error creating queue: %q", err)
	}
}

func main() {
	var conn *grpc.ClientConn
	connString := os.Getenv("QUEUE_URL")
	if connString == "" {
		connString = "localhost:50051"
	}
	fmt.Println("connecting with", connString)
	conn, err := grpc.Dial(connString, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()

	c := hello.NewHelloClient(conn)

	createQueue(c)
	consumeItems(c)
}
