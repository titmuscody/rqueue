package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"gitlab.com/titmuscody/rqueue/go_server/hello"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

const (
	// NAME of the queue
	NAME = "test topic"
)

func sendItems(c hello.HelloClient) {
	for i := 0; i < 100; i++ {
		// ctx := context.SetHeader(context.Background(), map[string]string{"topic": "test topic"})
		md := metadata.New(map[string]string{"topic": NAME})
		ctx := metadata.NewOutgoingContext(context.Background(), md)

		m := fmt.Sprintf("message %v", i)
		// log.Println("sending item", m)
		confirmation, err := c.AddItem(ctx, &hello.QueueItem{Message: m, Id: 0, Deadline: time.Now().Add(5 * time.Minute).UnixNano()})

		if err != nil {
			log.Fatalf("Error on AddItem: %q", err)
		}
		log.Printf("received send confirmation %v", confirmation.Id)
	}
}

func createQueue(c hello.HelloClient) {
	_, err := c.VerifyQueue(context.Background(), &hello.QueueProperties{Name: NAME, Durable: false})
	if err != nil {
		log.Fatalf("Error creating queue: %q", err)
	}
}

func main() {
	var conn *grpc.ClientConn
	connString := "localhost:50051"
	fmt.Println("connecting with", connString)
	conn, err := grpc.Dial(connString, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %s", err)
	}
	defer conn.Close()

	c := hello.NewHelloClient(conn)

	createQueue(c)

	sendItems(c)
}
