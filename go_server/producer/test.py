import grpc

import hello_pb2_grpc
import hello_pb2


with grpc.insecure_channel('127.0.0.1:50051') as chan:
    stub = hello_pb2_grpc.HelloStub(chan)
    stub.AddItem(hello_pb2.QueueItem(id=0, message='test message'))
