import time
import grpc

import hello_pb2_grpc
import hello_pb2

from locust import User, task, between


# class GrpcClient(hello_pb2_grpc.HelloStub):
#     """
#     Simple, sample XML RPC client implementation that wraps xmlrpclib.ServerProxy and
#     fires locust events on request_success and request_failure, so that all requests
#     gets tracked in locust's statistics.
#     """

#     _locust_environment = None

#     def __init__(self, chan):
#         super().__init__(self)
#         self.stub = hello_pb2_grpc.HelloStub(chan)


#     def __getattr__(self, name):
#         func = hello_pb2_grpc.HelloStub.__getattr__(self, name)
#         # func self=. .__getattr__(self, name)

#         def wrapper(*args, **kwargs):
#             start_time = time.time()
#             try:
#                 result = func(*args, **kwargs)
#             except Fault as e:
#                 total_time = int((time.time() - start_time) * 1000)
#                 self._locust_environment.events.request_failure.fire(
#                     request_type="grpc", name=name, response_time=total_time, exception=e
#                 )
#             else:
#                 total_time = int((time.time() - start_time) * 1000)
#                 self._locust_environment.events.request_success.fire(
#                     request_type="grpc", name=name, response_time=total_time, response_length=0
#                 )
#                 # In this example, I've hardcoded response_length=0. If we would want the response length to be
#                 # reported correctly in the statistics, we would probably need to hook in at a lower level

#         return wrapper


class GrpcUser(User):
    """
    This is the abstract User class which should be subclassed. It provides an XML-RPC client
    that can be used to make XML-RPC requests that will be tracked in Locust's statistics.
    """

    abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.client = GrpcClient(grpc.insecure_channel(self.host))
        self.client = hello_pb2_grpc.HelloStub(grpc.insecure_channel(self.host))
        self.client._locust_environment = self.environment
        self.message = 'test message' * 1000

class ApiUser(GrpcUser):
    host = "127.0.0.1:50051"
    wait_time = between(0.1, 1)

    @task()
    def create_item(self):
        start_time = time.time()

        data = hello_pb2.QueueItem(
            id=0,
            message=self.message,
        )
        self.client.AddItem(data, metadata=(('topic', 'test topic'),))

        dur = int((time.time() - start_time) * 1000)
        self.environment.events.request_success.fire(
            request_type='grpc',
            name='AddItem',
            response_time=dur,
            response_length=0,
        )
