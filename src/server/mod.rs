use futures_core::Stream;
use std::pin::Pin;
use rand::Rng;
use std::collections::BinaryHeap;
use std::collections::HashMap;
use std::sync::{Mutex, Arc};
use tonic::{transport::Server, Request, Response, Status};

use hello::hello_server::{Hello, HelloServer};
use hello::{QueueItem, Confirmation, ConsumerRequest, QueueProperties};
mod hello; 

type QueueHeap = BinaryHeap<std::cmp::Reverse<QueueItem>>;
type QueueType = (Arc<tokio::sync::Mutex<QueueHeap>>, QueueProperties);

// defining a struct for our service
pub struct QueueServer {
    queue_map: Mutex<HashMap<String, QueueType>>,
    pub pool: Option<Arc<redis::Client>>,
    delete_queue: tokio::sync::mpsc::Sender<Result<i32, redis::RedisError>>,
}

fn get_topic(meta: &tonic::metadata::MetadataMap) -> Result<&str, Status> {
    match meta.get("topic") {
        Some(val) => Ok(val.to_str().unwrap()),
        None => Err(tonic::Status::invalid_argument("topic must be specified")),
    }
}
fn get_queue_and_properties(queue_map: &Mutex<HashMap<String, QueueType>>, topic: &str) -> Result<QueueType, Status> {
    let glock = queue_map.lock().unwrap();
    match glock.get(topic) {
        Some((queue, properties)) => {
            Ok((Arc::clone(queue), properties.clone()))
        }
        None => return Err(tonic::Status::invalid_argument("queue does not exist")),
    }
}
// implementing rpc for service defined in .proto
#[tonic::async_trait]
impl Hello for QueueServer {
    type ConsumeItemsStream=Pin<Box<dyn Stream<Item = Result<QueueItem, Status>> + Send + Sync + 'static>>;

    async fn add_item(&self, request: Request<QueueItem>)->Result<Response<Confirmation>,Status>{
        let meta = request.metadata().clone();
        let topic = get_topic(&meta)?;
        let (gqueue, props) = get_queue_and_properties(&self.queue_map, topic)?;
        let mut queue_item = request.into_inner();
        let id = rand::thread_rng().gen::<i32>();
        if props.durable {
            let mut conn = self.pool.as_ref().unwrap().get_connection().unwrap();
            let db = serde_json::json!({
                "queue": topic,
                "id": id,
                "message": queue_item.message,
                "deadline": queue_item.deadline,
            });
            let exists: i64 = redis::cmd("HSETNX").arg("default".to_string()).arg(id).arg(db.to_string()).query(&mut conn).unwrap();
            if exists == 0 {
                return Err(tonic::Status::unavailable("collision detected. please try again".to_string()))
            }
        };
        queue_item.id = id;

        let mut q = gqueue.lock().await;
        q.push(std::cmp::Reverse(queue_item));
        drop(q);

        Ok(Response::new(Confirmation{id: id}))
    }
    async fn verify_queue(&self, request: Request<QueueProperties>) -> Result<Response<QueueProperties>, Status> {
        let properties = request.into_inner();
        if properties.durable {
            if self.pool.is_none() {
                return Err(tonic::Status::failed_precondition("durable queues are not enabled".to_string()))
            }
        }
        let mut queues = self.queue_map.lock().unwrap();
        match queues.get(&properties.name) {
            Some((_, props)) => {
                if props.durable != properties.durable {
                    return Err(tonic::Status::failed_precondition("property durable did not match".to_string()))
                }
            }
            None => {
                let new_queue = Arc::new(tokio::sync::Mutex::new(BinaryHeap::new()));
                queues.insert(properties.name.clone(), (new_queue, properties.clone()));
            }
        };
        Ok(Response::new(properties))
    }
    async fn consume_items(&self, request: Request<tonic::Streaming<ConsumerRequest>>)->Result<Response<Self::ConsumeItemsStream>,Status>{
        let meta = request.metadata().clone();
        let topic = get_topic(&meta)?;
        let mut streamer = request.into_inner();

        let mut queue: Vec<QueueItem> = vec![];
        let (mut gqueue, props) = get_queue_and_properties(&self.queue_map, topic)?;
        let mut delete_queue = self.delete_queue.clone();

        let output = async_stream::try_stream! {
            for next in get_work(&mut gqueue, &mut queue).await {
                yield next;
            }
            while let future = streamer.message().await {
                match future {
                    Ok(Some(req)) => {
                        match req.status.as_str() {
                            "ack" => {
                                let item = queue.pop().unwrap();
                                if props.durable {
                                    delete_queue.send(Ok(item.id)).await.unwrap();
                                }
                                for next in get_work(&mut gqueue, &mut queue).await {
                                    yield next;
                                }
                            }
                            "nack" => {
                                let mut q = gqueue.lock().await;
                                let item = queue.pop().unwrap();
                                q.push(std::cmp::Reverse(item));
                                // add_to_queue(&mut q, &mut queue)
                            }
                            "" => {
                                for next in get_work(&mut gqueue, &mut queue).await {
                                    yield next;
                                }
                            }
                            _ => {
                                println!("error unrecognized status received");
                            }
                        };
                    }
                    val => {
                        println!("error pipe {:?}", val);
                        let mut q = gqueue.lock().await;
                        for item in queue {
                            q.push(std::cmp::Reverse(item));
                        }
                        break;
                    }
                };
            }
        };
        Ok(Response::new(Box::pin(output)
            as Self::ConsumeItemsStream))
    }
}

async fn get_work(global_queue: &mut Arc<tokio::sync::Mutex<QueueHeap>>, local_queue: &mut Vec<QueueItem>) -> Vec<QueueItem> {
    let batch_size = 1;

    let mut output = vec![];
    let mut q = global_queue.lock().await;
    for _ in 0..batch_size-local_queue.len() {
        match q.pop() {
            Some(std::cmp::Reverse(item)) => {
                output.push(item.clone());
                local_queue.push(item);
            }
            None => break,
        }
    }
    output
}

pub fn new(db: Option<String>, channel_size: usize) -> Result<(QueueServer, tokio::sync::mpsc::Receiver<Result<i32, redis::RedisError>>), String> {
    let pool = create_pool(db)?;
    let queue_map = match &pool {
        Some(pool_val) => create_queue_map_from_db(Arc::clone(pool_val))?,
        None => Mutex::new(HashMap::new()),
    };
    let (delete_queue, rx) = tokio::sync::mpsc::channel::<Result<i32, _>>(channel_size);
    Ok((QueueServer{queue_map: queue_map, pool: pool, delete_queue: delete_queue}, rx))
}

fn create_pool(url: Option<String>) -> Result<Option<Arc<redis::Client>>, String> {
    Ok(match url {
        Some(url) => {
            // create db pool and migrate to correct db version
            match redis::Client::open(url) {
                Ok(pool) => Some(Arc::new(pool)),
                Err(_) => return Err("failed to open redis client".to_string())
            }
        }
        None => None,
    })
}

fn create_queue_map_from_db(pool: Arc<redis::Client>) -> Result<Mutex<HashMap<String, QueueType>>, String> {
    let conn_res = pool.get_connection();
    if conn_res.is_err() {
        return Err("could not get db connection".to_string())
    }
    let mut conn = conn_res.unwrap();
    let query_res = redis::cmd("HVALS").arg("default".to_string()).query(&mut conn);
    if query_res.is_err() {
        return Err("failed to run HVALS cmd in redis".to_string())
    }
    let messages: Vec<String> = query_res.unwrap();
    let mut queues: HashMap<String, Vec<QueueItem>> = HashMap::new();
    for json in messages {
        let json_res = serde_json::from_str(&json);
        if json_res.is_err() {
            return Err("failed to parse json from db response".to_string())
        }
        let row: serde_json::Value = json_res.unwrap();
        let name = row["queue"].as_str().unwrap().to_string();
        let queue_row = QueueItem{id: row["id"].as_u64().unwrap() as i32, message: row["message"].as_str().unwrap().to_string(), deadline: row["deadline"].as_u64().unwrap() as i64};
        let mut so_far = queues.get(&name).unwrap_or(&vec![]).clone();
        so_far.push(queue_row);
        queues.insert(name, so_far.clone());
    }
    // wrap up in required mutex and arcs for the server
    let queue_map = queues.iter().map(|(key, items)| {
        let reversed_heap: QueueHeap = items.iter().cloned().map(|cur| std::cmp::Reverse(cur)).collect();
        let pair = (Arc::new(tokio::sync::Mutex::new(reversed_heap)), QueueProperties{durable: false, name: key.clone()});
        (key.clone(), pair)
    }).collect();
    Ok(Mutex::new(queue_map))
}

pub async fn process_delete_queue(pool_option: Option<Arc<redis::Client>>, stream: Pin<&mut dyn Stream<Item = Result<i32, redis::RedisError>>>) {
    use futures::stream::TryStreamExt;
    const CONCURRENCY: usize = 10;
    if pool_option.is_none() {
        return;
    }
    let pool = &pool_option.unwrap();
    stream.try_for_each_concurrent(CONCURRENCY, |id| async move {
        let client = Arc::clone(pool);
        let mut conn = client.get_connection().unwrap();
        let queue = "default".to_string();
        let _: () = redis::cmd("HDEL").arg(queue).arg(id).query(&mut conn).unwrap();
        // println!("deleting db {}", id);
        Ok(())
    }).await.unwrap();
}

pub async fn run(url: String, queue_server: QueueServer) -> Result<(), tonic::transport::Error> {
    let addr = url.parse().unwrap();
    println!("Server listening on {}", addr);
    Server::builder()
        .add_service(HelloServer::new(queue_server))
        .serve(addr).await?;
    Ok(())
}
