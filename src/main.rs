
use std::sync::Arc;
use std::pin::Pin;

mod server;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {

    let pool_res = match std::env::var("DB_URL") {
        Ok(val) => Some(val),
        Err(_) => {
            println!("no address provided. durable queues have been disabled");
            None
        }
    };

    let (queue_server, mut rx) = server::new(pool_res, 4)?;
    let pinned = Pin::new(&mut rx);
    let queue_process = match &queue_server.pool {
        Some(pool_val) => server::process_delete_queue(Some(Arc::clone(pool_val)), pinned),
        None => server::process_delete_queue(None, pinned),
    };

    let server_process = server::run("0.0.0.0:50051".to_string(), queue_server);
    let (_, res) = futures::join!(queue_process, server_process);
    res?;
    Ok(())
}