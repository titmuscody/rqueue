package client

import (
	"context"
	"io"
	"log"
	"time"

	"gitlab.com/titmuscody/rqueue/go_server/hello"
	"google.golang.org/grpc/metadata"
)

type QueueConfig struct {
	Name string
}
type ClientConfig struct {
}

// type Config struct {
// 	QueueConfig
// 	ClientConfig
// }

type Client struct {
	qConfig QueueConfig
	cConfig ClientConfig
	client  hello.HelloClient
}

func (c *Client) Consume(f func(string)) error {
	md := metadata.New(map[string]string{"topic": c.qConfig.Name})
	ctx := metadata.NewOutgoingContext(context.Background(), md)
	stream, err := c.client.ConsumeItems(ctx)
	if err != nil {
		log.Fatalf("Error when calling Send: %s", err)
		return err
	}

	ticker := time.NewTicker(10 * time.Second)
	endChan := make(chan bool)
	go func() {
		for {
			select {
			case <-ticker.C:
				err := stream.Send(&hello.ConsumerRequest{Id: 0, Status: ""})
				if err != nil {
					log.Fatalf("error sending ping %q", err)
				}
			case <-endChan:
				return
			}
		}
	}()

	for {
		ticker.Reset(10 * time.Second)
		item, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Error receiving stream %q", err)
		}
		ticker.Stop()
		log.Printf("received topic=%v message=%v", item.Id, item.Message)
		// time.Sleep(1 * time.Second)
		err = stream.Send(&hello.ConsumerRequest{Id: item.Id, Status: "ack"})
		if err != nil {
			log.Fatalf("Error sending stream %q", err)
		}
	}
	ticker.Stop()
	endChan <- true
	return nil
}

func (c *Client) CreateQueue() {
}
